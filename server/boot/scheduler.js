/**
 * Created by Aniqa on 3/28/2016.
 */


var schedule = require('node-schedule');

module.exports = function(app) {
  var sql = app.dataSources.dsMySql;
  var DailyNotification = app.models.DailyNotification;
  var HourlyNotification = app.models.HourlyNotification;
  var Email = app.models.Email;

  function sendEmail(subject, row){
    var msg = "Given Below are your usage details:\n" +
              "Threshold value: " + row.notify_threshold + '\n' +
              "Watts:           " + row.watts;
    console.log('Sending Email to %s, Msg: %s', row.email, msg);

    Email.send({
      to: row.email,
      from: 'epat.zigron.com',
      subject: subject,
      html: msg
    }, function (err) {
      if (err)
        return console.log('> error sending hourly email', row.email);
      console.log('> sending email', row.email);
    });
  }

  function sendApp(row){

  }

  var j = schedule.scheduleJob('0 * * * *', function(){
  //var j = schedule.scheduleJob('* * * * *', function(){
    console.log('Hourly Sheduler Running Time: ', new Date());

    sql.connector.query("SELECT * FROM vw_hourly_notification", function (err, results) {
      if (err) {
        console.log('warning unable to execute query for hourly notification.');
      } else {

        console.log('Hourly Notification Result:', results);

        for(var i = 0; i < results.length; i++){
          var row = results[i];

          if(row.notify_email == '1')
            sendEmail('Hourly Email Notification', row);

          if(row.notify_app == '1')
            sendApp(row);
        }
      }
    });
  });

  var j = schedule.scheduleJob('0 0 * * *', function(){
    console.log('Daily Sheduler Running Time: ', new Date());

    sql.connector.query("SELECT * FROM vw_daily_notification", function (err, results) {
      if (err) {
        console.log('warning unable to execute query for dialy notification.');
      } else {

        console.log('Daily Notification Result:', results);

        for(var i = 0; i < results.length; i++){
          var row = results[i];

          if(row.notify_email == '1')
            sendEmail('Daily Email Notification', row);

          if(row.notify_app == '1')
            sendApp(row);
        }
      }
    });
  });
};
