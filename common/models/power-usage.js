module.exports = function (PowerUsage) {
    var app = require('../../server/epat');
    var baseValue = {};

    // Set Time
    PowerUsage.observe('before save', function (ctx, next) {
        ctx.instance.time = Math.round(new Date().getTime() / 1000);
        next();
    });

    // Get Old Power Entry Along With User Details
    PowerUsage.observe('before save', function (ctx, next) {
        PowerUsage.findOne({
            where: {userId: ctx.instance.userId},
            order: 'time DESC',
            include: 'user'
        }, function (err, power) {
            if (err) {
                console.log('warning unable to find old power entry.');
                ctx._old = null;
            } else {
                console.log('before power old', power);
                ctx._old = power;
            }
            next();
        });
    });

    // Add BaseValue for Pulse, detect restart of the board, detect restart of server
    PowerUsage.observe('before save', function (ctx, next) {
        if (ctx.instance.counter < ctx._old.counter) {
            baseValue[ctx.instance.userId] = ctx._old.pulses;
            console.log('base reset for user: %s, oldCounter: %s, counter: %s, baseVal: %s',
                ctx.instance.userId, ctx._old.counter, ctx.instance.counter, ctx._old.pulses);
        }
        if (baseValue[ctx.instance.userId])
            ctx.instance.pulses = baseValue[ctx.instance.userId] + ctx.instance.counter;
        else
            ctx.instance.pulses = ctx.instance.counter
        next();
    });

};
