var path = require('path');
var request = require('request');
var cheerio = require('cheerio');
var app = require('../../server/epat');
var config = require('../../server/config.json');

module.exports = function(UserEpat) {

  var sql = app.dataSources.dsMySql;

  function getCurrentUserId() {
    var ctx = app.getCurrentContext();
    var accessToken = ctx && ctx.get('accessToken');
    var userId = accessToken && accessToken.userId;
    return userId;
  }

  UserEpat.bill = function (id, cb) {
    var Bill = app.models.Bill;

    UserEpat.findOne({where: {id : id}}, function (err, user) {
      if (err) {
        cb(err, user);
      }else{
        console.log('User', user.referenceNo);
        Bill.getBill(user.referenceNo, function(err, body){
          if(err){
            cb(err, body);
          }else {
            var $ = cheerio.load(body);
            //console.log('bill:', body)
            var res = {
              past : [
                {
                  month: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > font').text().trim(),
                  units: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > font').text().trim(),
                  bill: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > font').text().trim(),
                  payment: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(2) > td:nth-child(5) > div > font').text().trim()
                },
                {
                  month: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > font').text().trim(),
                  units: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > font').text().trim(),
                  bill: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > font').text().trim(),
                  payment: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(3) > td:nth-child(5) > div > font').text().trim()
                },
                {
                  month: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > font').text().trim(),
                  units: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > font').text().trim(),
                  bill: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(4) > td:nth-child(4) > div > font').text().trim(),
                  payment: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(4) > td:nth-child(5) > div > font').text().trim()
                },
                {
                  month: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(5) > td:nth-child(1) > div > font').text().trim(),
                  units: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(5) > td:nth-child(3) > div > font').text().trim(),
                  bill: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(5) > td:nth-child(4) > div > font').text().trim(),
                  payment: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(5) > td:nth-child(5) > div > font').text().trim()
                },
                {
                  month: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(6) > td:nth-child(1) > div > font').text().trim(),
                  units: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(6) > td:nth-child(3) > div > font').text().trim(),
                  bill: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(6) > td:nth-child(4) > div > font').text().trim(),
                  payment: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(6) > td:nth-child(5) > div > font').text().trim()
                },
                {
                  month: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(7) > td:nth-child(1) > div > font').text().trim(),
                  units: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(7) > td:nth-child(3) > div > font').text().trim(),
                  bill: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(7) > td:nth-child(4) > div > font').text().trim(),
                  payment: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(7) > td:nth-child(5) > div > font').text().trim()
                },
                {
                  month: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(8) > td:nth-child(1) > div > font').text().trim(),
                  units: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(8) > td:nth-child(3) > div > font').text().trim(),
                  bill: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(8) > td:nth-child(4) > div > font').text().trim(),
                  payment: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(8) > td:nth-child(5) > div > font').text().trim()
                },
                {
                  month: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(9) > td:nth-child(1) > div > font').text().trim(),
                  units: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(9) > td:nth-child(3) > div > font').text().trim(),
                  bill: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(9) > td:nth-child(4) > div > font').text().trim(),
                  payment: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(9) > td:nth-child(5) > div > font').text().trim()
                },
                {
                  month: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(10) > td:nth-child(1) > div > font').text().trim(),
                  units: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(10) > td:nth-child(3) > div > font').text().trim(),
                  bill: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(10) > td:nth-child(4) > div > font').text().trim(),
                  payment: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(10) > td:nth-child(5) > div > font').text().trim()
                },
                {
                  month: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(11) > td:nth-child(1) > div > font').text().trim(),
                  units: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(11) > td:nth-child(3) > div > font').text().trim(),
                  bill: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(11) > td:nth-child(4) > div > font').text().trim(),
                  payment: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(11) > td:nth-child(5) > div > font').text().trim()
                },
                {
                  month: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(12) > td:nth-child(1) > div > font').text().trim(),
                  units: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(12) > td:nth-child(3) > div > font').text().trim(),
                  bill: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(12) > td:nth-child(4) > div > font').text().trim(),
                  payment: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(12) > td:nth-child(5) > div > font').text().trim()
                },
                {
                  month: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(13) > td:nth-child(1) > div > font').text().trim(),
                  units: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(13) > td:nth-child(3) > div > font').text().trim(),
                  bill: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(13) > td:nth-child(4) > div > font').text().trim(),
                  payment: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(13) > td:nth-child(5) > div > font').text().trim()
                }
              ],
              curr : [
                {name: 'Bill Month', val: $('body > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > font').text().trim()},
                {name: 'Reading Date', val: $('body > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr > td.unnamed1 > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(4)').text().trim()},
                {name: 'Issue Date', val: $('body > table:nth-child(2) > tbody > tr:nth-child(1) > td > table > tbody > tr > td.unnamed1 > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(5)').text().trim()},
                {name: 'Due Date', val: $('body > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > font').text().trim()},
                {name: 'Referrence No.', val: $('body > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > font').text().trim()},
                {name: 'Connection Date', val: $('body > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td.GeneralText').text().trim()},
                {name: 'Meter No.', val: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div').text().trim()},
                {name: 'Previous Reading', val: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div').text().trim()},
                {name: 'Present Reading', val: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div').text().trim()},
                {name: 'Units Consumed', val: $('body > table:nth-child(2) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(5) > div').text().trim()},
                {name: 'GST', val: $('body > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > font').text().trim()},
                {name: 'PTV Fee', val: $('body > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div').text().trim()},
                {name: 'Bill Adj', val: $('body > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(2) > td:nth-child(5) > div').text().trim()},
                {name: 'Current Bill', val: $('body > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(1) > td.GeneralText > div > font').text().trim()},
                {name: 'Amount Within Due Date', val: $('body > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.GeneralText > div > font').text().trim()},
                {name: 'L.P Surcharge', val: $('body > table:nth-child(2) > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(5) > td.GeneralText > div > font').text().trim()},
                {name: 'Amount After Due Date', val: $('body > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(6) > div > font').text().trim()}
              ]
            }
            console.log('Bill: ', res);
            cb(err, res);
          }
        })
      }
    });
  }

  UserEpat.remoteMethod(
    'bill',
    {
      accepts: [
        {arg: 'id', type: 'number'}
      ],
      returns: { type: 'object', root: true },
      http: {path: '/bill', verb: 'get'}
    }
  );

  UserEpat.projectedBill = function (id, cb) {

    var query = "SELECT p.units_projected, u.phase_type phase, u.connection_type conn " +
      "FROM vw_projected_units p JOIN user_epat u ON p.id = u.id " +
      "WHERE p.id=" + id + ";";

    sql.connector.query(query, function (err, data) {
      if (err) {
        cb(err, data);
      }else{
        console.log("projected units:", data[0]);
        request.post('http://64.17.175.168/Modules/BillCalc/CalculateBill.asp', {form:{
          selPhase: data[0].phase,
          selConn: data[0].conn,
          txtUnits: parseInt(data[0].units_projected),
          txtMR: 0,
          txtSR: 0,
          txtInst: 0,
          txtUnpDebt: 0,
          txtArears: 0,
          chkPTVFee: true,
          Submit: 'Submit'
        }}, function(err, httpResponse, body){
          //console.log("projected bill:", err, body);
          if(err){
            cb(err, body);
          }else {
            var $ = cheerio.load(body);
            var res = [
              {name: 'Estimated Units', val: $('body > table > tr:nth-child(2) > td:nth-child(3) > table:nth-child(3) > tr:nth-child(1) > td:nth-child(2)').text().trim()},
              {name: 'Total Cost of Electricity', val: $('body > table > tr:nth-child(2) > td:nth-child(3) > table:nth-child(3) > tr:nth-child(2) > td:nth-child(2)').text().trim()},
              {name: 'Electricity Duty', val: $('body > table > tr:nth-child(2) > td:nth-child(3) > table:nth-child(3) > tr:nth-child(3) > td:nth-child(2)').text().trim()},
              {name: 'NJS', val: $('body > table > tr:nth-child(2) > td:nth-child(3) > table:nth-child(3) > tr:nth-child(4) > td:nth-child(2)').text().trim()},
              {name: 'GST', val: $('body > table > tr:nth-child(2) > td:nth-child(3) > table:nth-child(3) > tr:nth-child(5) > td:nth-child(2)').text().trim()},
              {name: 'Income Tax', val: $('body > table > tr:nth-child(2) > td:nth-child(3) > table:nth-child(3) > tr:nth-child(6) > td:nth-child(2)').text().trim()},
              {name: 'PTV Fee', val: $('body > table > tr:nth-child(2) > td:nth-child(3) > table:nth-child(3) > tr:nth-child(9) > td:nth-child(2)').text().trim()},
              {name: 'Amount Within Due Date', val: $('body > table > tr:nth-child(2) > td:nth-child(3) > table:nth-child(3) > tr:nth-child(10) > td:nth-child(2)').text().trim()},
              {name: 'L.P Surcharge', val: $('body > table > tr:nth-child(2) > td:nth-child(3) > table:nth-child(3) > tr:nth-child(14) > td:nth-child(2)').text().trim()},
              {name: 'Amount After Due Date', val: $('body > table > tr:nth-child(2) > td:nth-child(3) > table:nth-child(3) > tr:nth-child(15) > td:nth-child(2)').text().trim()}
            ]
            console.log('ProjectedBill: ', res);
            cb(err, res);
          }
        })
      }
    });
  }

  UserEpat.remoteMethod(
    'projectedBill',
    {
      accepts: [
        {arg: 'id', type: 'number'}
      ],
      returns: { type: 'array', root: true },
      http: {path: '/projectedBill', verb: 'get'}
    }
  );

  //send verification email after registration
  UserEpat.afterRemote('create', function(context, UserEpat, next) {
    console.log('> user.afterRemote triggered');

    var options = {
      type: 'email',
      to: UserEpat.email,
      from: 'noreply@loopback.com',
      subject: 'Thanks for registering.',
      template: path.resolve(__dirname, '../../server/views/verify.ejs'),
      redirect: '/verified',
      user: UserEpat,
      host: 'epat.zigron.com',
      port: 80
    };

    UserEpat.verify(options, function(err, response) {
      if (err) return next(err);

      console.log('> verification email sent:', response);

      context.res.render('response', {
        title: 'Signed up successfully',
        content: 'Please check your email and click on the verification link ' +
        'before logging in.',
        redirectTo: '/',
        redirectToLinkText: 'Log in'
      });
    });
  });

  //send password reset link when requested
  UserEpat.on('resetPasswordRequest', function(info) {
    var url = 'http://epat.zigron.com/reset-password';
    var html = 'Click <a href="' + url + '?access_token=' +
      info.accessToken.id + '">here</a> to reset your password';

    UserEpat.app.models.Email.send({
      to: info.email,
      from: info.email,
      subject: 'Password reset',
      html: html
    }, function(err) {
      if (err) return console.log('> error sending password reset email');
      console.log('> sending password reset email to:', info.email);
    });
  });
};

